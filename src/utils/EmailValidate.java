/**
 * Ze stránky https://www.java2novice.com/java-collections-and-util/regex/valid-email-address/
 */
package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class EmailValidate {
 
    private static final Pattern emailNamePtrn = Pattern.compile(
    "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
     /**
      * Metoda pro kontrolu mailu pomocí regulárního výrazu
      * @param mail kontrolovaný mail
      * @return vrací úspěch kontroly
      */
    public static boolean validateEmailAddress(String mail){
         
        Matcher mtch = emailNamePtrn.matcher(mail);
        return mtch.matches();
    }
     
    public static void main(String a[]){
        System.out.println("Is 'java2novice@gmail.com' a valid email address? "
                                    +validateEmailAddress("java2novice@gmail.com"));
        System.out.println("Is 'cric*7*&@yahoo.com' a valid email address? "
                                    +validateEmailAddress("cric*7*&@yahoo.com"));
        System.out.println("Is 'JAVA2NOVICE.gmail.com' a valid email address? "
                                    +validateEmailAddress("JAVA2NOVICE.gmail.com"));
    }
}