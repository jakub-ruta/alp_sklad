/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;
import app.Zbozi;
import java.util.Comparator;

/**
 *
 * @author jakub.ruta
 */
public class ComparatorByEan implements Comparator<Zbozi> {

    @Override
    public int compare(Zbozi o1, Zbozi o2) {
        return o1.getEan() - o2.getEan();
    }
    
}
