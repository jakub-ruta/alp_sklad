package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpURLConnectionExample {

	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String POST_URL = "http://arduino.topescape.cz/skola/mail.php";	
        /**
         * Funkce pro zaslání POST dotazu na server, který následně odešle mail na zadanou adresu s tělem zprávy daných dat
         * ukradeno ze stránky https://www.journaldev.com/7148/java-httpurlconnection-example-java-http-request-get-post
         * @param mail na který se odešle
         * @param data data, která mají být odeslána
         * @throws IOException vyjímka 
         */
	public static void sendPOST(String mail, String data) throws IOException {
	String POST_PARAMS = "mail="+mail + "&data=" + data;	
            URL obj = new URL(POST_URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);

		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(POST_PARAMS.getBytes());
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		//System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { //success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			//System.out.println(response.toString());
		} else {
			//System.out.println("POST request not worked");
		}
	}

}