/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 * Rozhraní pro definici práce se soubory
 * @author jakub
 */
public  interface saveable {
    /**
     * 
     * @param soubor název
     * @param data pole Stringů s daty na vložení do souboru, každý String na jeden řádek
     * @return vrací úsopěch
     */
    public boolean saveData(String soubor, String [] data);
   /**
    * 
    * @param soubor název souboru
    * @return vrací úspěch
    */
    public boolean loadData(String soubor);
}
