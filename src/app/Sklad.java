/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import utils.ComparatorByEan;
import utils.HttpURLConnectionExample;
import utils.saveable;

/**
 *
 * @author jakub
 */
public class Sklad implements saveable {

    public final String nazevSkladu;

    private ArrayList<Zbozi> poleZbozi = new ArrayList();

    /**
     * konstruktor pro sklad
     *
     * @param nazevSkladu název skladu
     */
    public Sklad(String nazevSkladu) {
        this.nazevSkladu = nazevSkladu;
        nacteniSkladu();
    }

    /**
     * funkce pro výdej zboží ze skladu. V případě požadavku více zobží než je
     * naskladněno vrací false a nevykoná požadavek
     *
     * @param Ean Ean položky
     * @param pocet vydaných kusů
     * @return úspěch
     */
    public boolean vydej(int Ean, int pocet) {
        for (Zbozi u : poleZbozi) {
            if (u.getEan() == Ean) {
                if (u.getPocetNaSkladu() - pocet < 0) {
                    return false;
                }
                u.setPocetNaSkladu(u.getPocetNaSkladu() - pocet);
                log("vydej - " + Ean + " - " + pocet);
                return true;
            }
        }
        return false;
    }

    /**
     * funkce pro načtení skladu ze souboru pro inicializaci
     */
    private void nacteniSkladu() {
        loadData("data.txt");
    }

    /**
     * Funkce pro příjem do skladu - v případě, že Ean ještě není na skladě,
     * založí se nová položka
     *
     * @param Ean Ean položky
     * @param pocet položek
     * @return úspěch
     */
    public boolean prijem(int Ean, int pocet) {
        for (Zbozi u : poleZbozi) {
            if (u.getEan() == Ean) {
                u.setPocetNaSkladu(u.getPocetNaSkladu() + pocet);
                log("prijem - " + Ean + " - " + pocet);
                return true;
            }
        }
        poleZbozi.add(Zbozi.getInstance(Ean, pocet));
        log("Nova polozka - " + Ean + " - " + pocet);
        return true;
    }

    /**
     * Nalezení zboží podle Ean
     *
     * @param Ean Ean položky
     * @return vypíše parametry zboží (EAN, název, počet na skladě)
     */
    public String getZboziByEan(int Ean) {
        for (Zbozi u : poleZbozi) {
            if (u.getEan() == Ean) {
                return u.toString();
            }
        }
        return null;
    }

    public String[] getAll() {
        int i = 0;
        String[] str = new String[poleZbozi.size()];
        for (Zbozi u : poleZbozi) {
            str[i] = u.toStringMass();
            i++;
        }
        return str;
    }

    /**
     * funkce na uložení dat do souboru, skládá pole a volá další funkci
     * ulozdata
     *
     * @param soubor název souboru
     * @return úspěch
     */
    public boolean ulozData(String soubor) {
        int i = 0;
        String[] str = new String[poleZbozi.size()];
        for (Zbozi u : poleZbozi) {
            str[i] = u.toStringMass();
            i++;
        }
        return saveData(soubor, str);
    }

    /**
     * implementace rozhraní pomocí bufrwrtiteru zapisuje do souboru (půvosní
     * přepíše)
     *
     * @param soubor název souboru
     * @param data data na zapsání, každý řádek pole na svůj řádek v souboru
     * @return úspěch
     */
    @Override

    public boolean saveData(String soubor, String[] data) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(soubor))) {
            for (Zbozi u : poleZbozi) {
                bw.write(u.toStringMass());
                bw.newLine();
            }
            bw.flush();
        } catch (Exception e) {
            System.err.println("Do souboru se nepovedlo zapsat.");
            return false;
        }
        return true;
    }

    /* public boolean saveData(String soubor, String[] data) {
        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(soubor + ".dat"))) {
            for (Zbozi u : poleZbozi) {
                out.writeUTF(u.toStringMass());
                
            }
            out.close();
        } catch (Exception e) {
            System.err.println("Do souboru se nepovedlo zapsat.");
            return false;
        }
        return true;
    }*/
    /**
     * načítá do pole zadaný soubor
     *
     * @param soubor název souboru
     * @return úspěch
     */
    @Override
    public boolean loadData(String soubor) {
        try (BufferedReader br = new BufferedReader(new FileReader(soubor))) {
            String s;
            poleZbozi.clear();
            while ((s = br.readLine()) != null) {
                s = s.replace("\t", " ");
                String[] str = s.split(" ");

                poleZbozi.add(Zbozi.getInstance(Integer.parseInt(str[0]), str[1], Integer.parseInt(str[2])));
            }
            br.close();
        } catch (Exception e) {
            System.err.println("Chyba při četení ze souboru.");
            return false;
        }
        return true;
    }

    /* public boolean loadData(String soubor) {
        try (DataInputStream in = new DataInputStream(new FileInputStream(soubor + ".dat"))) {
            String s;
            poleZbozi.clear();
            boolean end = true;
            while (!end) {
                try {
                    s = in.readUTF();
                    s = s.replace("\t", " ");
                    String[] str = s.split(" ");

                    poleZbozi.add(Zbozi.getInstance(Integer.parseInt(str[0]), str[1], Integer.parseInt(str[2])));
                            
                } catch (EOFException e) {
                    end = true;
                }                
            }           
            in.close();
        } catch (Exception e) {
            System.err.println("Chyba při četení ze souboru.");
            return false;
        }
        return true;
    }
     */
    /**
     * funkce pro nastavení názvu zboží
     *
     * @param Ean Ean položky
     * @param nazev nový název položky
     * @return úspěch
     */
    public boolean setNazevZbozi(int Ean, String nazev) {
        for (Zbozi u : poleZbozi) {
            if (u.getEan() == Ean) {
                u.setNazevZbozi(nazev);
                return true;
            }
        }
        return false;
    }

    /**
     * seřazení podle počtu na skladě, využívá buble sort
     */
    public void sortByPocetNaSklade() {
        Collections.sort(poleZbozi);
        /** for (int i = 0; i < poleZbozi.size() - 1; i++) {
            for (int j = 0; j < poleZbozi.size() - i - 1; j++) {
                if (poleZbozi.get(j).compare(poleZbozi.get(j), poleZbozi.get(j + 1)) > 0) {
                    Zbozi tmp = poleZbozi.get(j);
                    poleZbozi.set(j, poleZbozi.get(j + 1));
                    poleZbozi.set(j + 1, tmp);
                }
            }
        }*/
    }

    /**
     * Bublinkove razeni (od nejvyssiho) seřadí pole podle Eanu
     *
     */
    public void sortByEan() {
        Collections.sort(poleZbozi, new ComparatorByEan());
        
        /**for (int i = 0; i < poleZbozi.size() - 1; i++) {
            for (int j = 0; j < poleZbozi.size() - i - 1; j++) {
                if (poleZbozi.get(j).compareTo(poleZbozi.get(j + 1)) > 0) {
                    Zbozi tmp = poleZbozi.get(j);
                    poleZbozi.set(j, poleZbozi.get(j + 1));
                    poleZbozi.set(j + 1, tmp);
                }
            }
        }*/
    }

    /**
     * Funkce pro zavolání metody na vytvoření POST dotazu na server, který
     * odešle mail + vytvoření sestavy dat - invetury
     *
     * @param mail emial na který se má data odeslat
     * @return úspěch vytvoření dotazu
     */
    public boolean sendMail(String mail) {
        try {
            StringBuilder s = new StringBuilder();
            s.append("Ean \t Název  \t Počet na skladě\n");
            for (Zbozi u : poleZbozi) {
                s.append(u.toStringMass()).append("\n");
            }

            HttpURLConnectionExample.sendPOST(mail, s.toString());
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     * funkce na logování do souboru, loguje ve tvaru aktuální čas, a vložená
     * data
     *
     * @param data data do souboru
     */
    public static void log(String data) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("log.txt", true))) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            bw.write(dtf.format(now));
            bw.write("\t " + data);
            bw.newLine();
            bw.flush();
        } catch (Exception e) {

        }

    }

}
