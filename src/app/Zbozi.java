/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

/**
 *
 * @author jakub
 */
public class Zbozi implements Comparable<Zbozi>{
    private final int Ean;
    private String nazevZbozi;
    private int pocetNaSkladu;

    public Zbozi(int Ean, String nazevZbozi, int pocetNaSkladu) {
        this.Ean = Ean;
        this.nazevZbozi = nazevZbozi;
        this.pocetNaSkladu = pocetNaSkladu;
    }

    public Zbozi(int Ean, int pocetNaSkladu) {
        this.Ean = Ean;
        this.pocetNaSkladu = pocetNaSkladu;
    }

    public void setNazevZbozi(String nazevZbozi) {
        this.nazevZbozi = nazevZbozi;
    }

    public int getEan() {
        return Ean;
    }

    public String getNazevZbozi() {
        return nazevZbozi;
    }

    public int getPocetNaSkladu() {
        return pocetNaSkladu;
    }

    public void setPocetNaSkladu(int pocetNaSkladu) {
        this.pocetNaSkladu = pocetNaSkladu;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Ean: \t").append(Ean);
        if(Ean < 9999999){
            str.append("\t");
        }
        str.append("\nNázev zboží: \t").append(nazevZbozi);
        str.append("\nPocet na skaldu: \t").append(pocetNaSkladu);
        return str.toString();
    }
    /**
     * funkce to strig s formátováním na řádek a bez popisu
     * @return  string (formátovaný na tři řádky
     */
    public String toStringMass() {
        StringBuilder str = new StringBuilder();
        str.append(Ean).append("\t");
        str.append(nazevZbozi);
        str.append("\t").append(pocetNaSkladu);
        return str.toString();
    }
    
    
    static Zbozi getInstance(int Ean, String nazev, int pocet) {
        return new Zbozi(Ean, nazev, pocet);
    }

    static Zbozi getInstance(int Ean, int pocet) {
        return new Zbozi(Ean, pocet);
    }

    @Override
    public int compareTo(Zbozi t) {
        return pocetNaSkladu - t.pocetNaSkladu;
    }     

    
    
}
