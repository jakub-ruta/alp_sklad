/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import app.Sklad;
import java.util.Scanner;
import utils.EmailValidate;

/**
 *
 * @author jakub
 */
public class Main {

    /** Hlavní main programu - UI
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Sklad brno = new Sklad("Brno");
        while (true) {
            printMainMenu();
            int volba;
            if(sc.hasNextInt()){
                volba = sc.nextInt();
            }
            else{
                volba = 0;
                sc.nextLine();
            }
            
            switch (volba) {
                case 1: {
                    System.out.println("***********Výdej zboží*************");
                    System.out.println("Zadej Ean položky:");
                    int Ean;
                    while(true){
                        if(sc.hasNextInt()){
                            Ean = sc.nextInt();
                            break;
                        }
                        sc.nextLine();
                        System.out.println("Chyba, zkus to zadat znovu:");
                    }
                    
                    System.out.println("Zadej počet vydaných kusů");
                    int pocet;
                    while(true){
                        if(sc.hasNextInt()){
                            pocet = sc.nextInt();
                            break;
                        }
                        sc.nextLine();
                        System.out.println("Chyba, zkus to zadat znovu:");
                    }
                    System.out.println("Výdej proběhl " + (brno.vydej(Ean, pocet) ? "úspěšně" : "neúspěšně - není na skladě nebo neexistuje") + ".");
                    System.out.println("************************************");
                }
                break;
                case 2: {
                    System.out.println("***********Příjem zboží*************");
                    System.out.println("Zadej Ean položky:");
                    int Ean;
                    while(true){
                        if(sc.hasNextInt()){
                            Ean = sc.nextInt();
                            break;
                        }
                        sc.nextLine();
                        System.out.println("Chyba, zkus to zadat znovu:");
                    }
                    System.out.println("Zadej počet přijatých kusů");
                    int pocet;
                    while(true){
                        if(sc.hasNextInt()){
                            pocet = sc.nextInt();
                            break;
                        }
                        sc.nextLine();
                        System.out.println("Chyba, zkus to zadat znovu:");
                    }
                    System.out.println("Příjem proběhl " + (brno.prijem(Ean, pocet) ? "úspěšně" : "neúspěšně") + ".");
                    System.out.println("************************************");
                }
                break;
                case 3: {
                    System.out.println("***********Zobrazení detailu********");
                    System.out.println("Zadej Ean položky:");
                    int Ean;
                    while(true){
                        if(sc.hasNextInt()){
                            Ean = sc.nextInt();
                            break;
                        }
                        sc.nextLine();
                        System.out.println("Chyba, zkus to zadat znovu:");
                    }
                    String polozka = brno.getZboziByEan(Ean);
                    if (polozka == null) {
                        System.out.println("Zboží neexistuje!");
                    } else {
                        System.out.println(polozka);
                    }
                    System.out.println("************************************");
                }
                break;
                case 4: {
                    System.out.println("***********Úprava položky***********");
                    System.out.println("Zadej Ean položky:");
                    int Ean;
                    while(true){
                        if(sc.hasNextInt()){
                            Ean = sc.nextInt();
                            break;
                        }
                        sc.nextLine();
                        System.out.println("Chyba, zkus to zadat znovu:");
                    }
                    String polozka = brno.getZboziByEan(Ean);
                    if (polozka == null) {
                        System.out.println("Zboží neexistuje!");
                    } else {
                        System.out.println(polozka);
                    }
                    System.out.println("Zadej nový název:");
                    String nazev = sc.next();
                    brno.setNazevZbozi(Ean, nazev);
                    System.out.println("************************************");
                }
                break;
                case 5: {
                    System.out.println("***********Inventura****************");
                    String[] str = brno.getAll();
                    System.out.println("Ean \t Název \t Počet na skladě");
                    for (String u : str) {
                        System.out.println(u);
                    }
                    
                    System.out.println("Odeslat invetura mailem? A/N");
                    if(sc.next().matches("A")){
                        System.out.println("Zadej mail");
                        do {
                            String mail = sc.next();                        
                        if (EmailValidate.validateEmailAddress(mail)){
                            brno.sendMail(mail);
                            break;
                        }
                        else{
                            System.out.println("Naplatn mail, zadej znovu:");
                        }
                        }while(true);
                        
                        
                    }
                    System.out.println("************************************");
                }
                break;
                case 6: {
                    System.out.println("***********Sařadit****************");

                    System.out.println("Seřadit podle Eanu (E) nebo podle počtu na skladě (P)?");
                    String sort = sc.next();
                    if (sort.matches("E")) {
                        brno.sortByEan();
                    } else {
                        brno.sortByPocetNaSklade();
                    }
                    System.out.println("************************************");
                }
                break;
                case 7: {
                    System.out.println("***********Export****************");

                    System.out.println("Zadej název souboru");
                    String nazev = sc.next();
                    System.out.println("Export proběhl " + (brno.ulozData(nazev) ? "úspěšně" : "neúspěšně") + ".");
                    System.out.println("************************************");
                }
                break;
                case 8: {
                    System.out.println("***********Import****************");

                    System.out.println("Zadej název souboru");
                    String nazev = sc.next();
                    System.out.println("Import proběhl " + (brno.loadData(nazev) ? "úspěšně" : "neúspěšně") + ".");
                    System.out.println("************************************");
                }
                break;

                case 9: {
                    System.out.println("Ukoncovani programu");
                    brno.ulozData("data.txt");
                    System.out.println("Program ukoncen");
                    return;
                }
                default:
                    System.out.println("Opakuj volbu v menu");
                    break;

            }
        }
    }
/**
 * Pro vypsání menu na systémový výstup
 */
    public static void printMainMenu() {
        System.out.println("*************Menu************");
        System.out.println("* 1 - výdej zboží           *");
        System.out.println("* 2 - příjem zboží          *");
        System.out.println("* 3 - zobrazit položku      *");
        System.out.println("* 4 - upravit položku       *");
        System.out.println("* 5 - inventura             *");
        System.out.println("* 6 - seřadit               *");
        System.out.println("* 7 - export                *");
        System.out.println("* 8 - import                *");
        System.out.println("* 9 - ukončit program       *");
        System.out.println("*****************************");
    }

    

}
